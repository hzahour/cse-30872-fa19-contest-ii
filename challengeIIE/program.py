#!/usr/bin/env python3

# tree.py

import sys

# GLOBAL VARIABLES

PATHS = []

# FUNCTIONS

def traverse(tree,index,path,target):
    
    il = (2*index) + 1
    ir = (2*index) + 2  
    
    stopl = False
    stopr = False

    if il >= len(tree) or tree[il] == 0:
      stopl = True
    
    if ir >= len(tree) or tree[ir] == 0:
      stopr = True

    if sum(path) == target and stopl and stopr:
      PATHS.append(path)
      return
    elif stopl and stopr:
      return

    path1 = list(path)
    path2 = list(path)
   
    if not stopl:
      path1.append(tree[il])
    if not stopr:
      path2.append(tree[ir])

    traverse(tree,il,path1,target)
    traverse(tree,ir,path2,target)
    
if __name__ == "__main__":

    for target in sys.stdin:
        
        target = int(target.strip())
        tree = [int(x) for x in sys.stdin.readline().strip().split()]
        
        p = []
        p.append(tree[0])
        p = list(p)
        traverse(tree,0,p,target)
        
        for path in sorted(PATHS):
            print("{}: {}".format(target,", ".join([str(n) for n in path])))

        PATHS.clear()
