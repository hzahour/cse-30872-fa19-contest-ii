#!/usr/bin/env python3

import sys
import math

solu = []

def different_letters(w1, w2):

    if len(w1) == len(w2):
        diff_letter = 0
        for i in range(len(w1)):
            if w1[i] != w2[i]:
                diff_letter += 1
        return diff_letter == 1

    else:
        x = max(w1, w2, key=len)
        y = min(w1, w2, key=len)
        
        return x[1:] == y or x[:-1] == y  


def morphy(graph, item, directed):
    global solu

    if not item:
        return item
	
    if item in graph:
        for n in graph[item]:
            morphy(graph, n, directed + [n])
 
    if len(directed) > len(solu):
        solu = directed
    return solu

if __name__ == '__main__':

    #parameters
    words = []

    #here, we are doing the input 
    for line in sys.stdin:
        line = line.strip()
        #line = line.lower()
        if line:
            words.append(line)  
      
    if not words:
        exit(0)

    words = sorted(words)

	#we are going to create a graph where all the adjacent words are 
    #bigger in size than the word that we look at


    graph = {}

    for word in words:
        for w2 in words:
            if different_letters(word, w2) == 1 and (word < w2):     
                if word not in graph:
                    graph[word] = []
                graph[word].append(w2)    

    if not graph:
        print ("1")
        print (words[0])
        exit(0)

    morphys = []
    for item in graph.keys():
        if graph[item]:
            solu = [item] + morphy(graph, item, [])
            pathToAdd = solu.copy()
            morphys.append(pathToAdd)
            solu.clear()

    solution = max(morphys, key=len)
  

    print(len(solution))
    for word in solution:
        print(word)
