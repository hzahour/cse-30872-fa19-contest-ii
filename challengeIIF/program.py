#!/usr/bin/env python3
import sys
import collections 

COUPLES = {} 
CP			= collections.defaultdict(set) # child to parent 
PC 			= collections.defaultdict(set) # parent to child 

def create_couples(inputs): 
	
	for line in inputs: 
		parents = line.split(':')[0]
		p1, p2  = parents.split(' ')
		COUPLES[p1] = p2
		COUPLES[p2] = p1

def create_cp_pc(inputs): 
		
	for line in inputs:
		
		'''try: 
			parents, children = line.split(': ')
			parents 					= parents.split(' ')	
			children 					= children.split(' ') 
		except ValueError: 
			children = []'''

		desc 			= line.split(':')
		parents 	= desc[0]
		parents 	= parents.split()
		children 	= desc[1]

		if children: 
			children = children.strip().split()
		else:
			children = []

		PC[parents[0]] 		=	set(children)
		PC[parents[1]] 		= set(children)
		
		for kid in children: 
			CP[kid].add(parents[0]) 
			CP[kid].add(parents[1])
	
if __name__=="__main__": 

	# Read in input 
	for line in sys.stdin: 
		
		n_fams = int(line.strip())
		inputs = []	
		
		if n_fams == 0: 
			break

		# Get tree description from input, store in list 		
		for _ in range(n_fams): 
			inputs.append(sys.stdin.readline().strip()) 
			
		create_couples(inputs)
		create_cp_pc(inputs)
	
		# Read in givers
		n_givers = int(sys.stdin.readline().strip()) 

		for _ in range(n_givers): 

			children_gifts= set()
			parent_gifts  = set()
			giver 				= sys.stdin.readline().strip()
			giver_spouse 	= None
			if giver in COUPLES: 
				giver_spouse = COUPLES[giver]

			#print("GIVER: ", giver)
			#print("GIVER_SPOUSE: ", giver_spouse)
		
			parents = CP[giver]

			if giver_spouse:  
				for parent_ in CP[giver_spouse]:
					parents.add(parent_)
	
			#print("PARENTS OF THE GIVER: ", parents)

			# Loop through total_parents and get there children
			for parent in parents: 

				#parent_spouse = COUPLES[parent]

				for child_ in PC[parent]:
					if child_ != giver and child_ != giver_spouse: 
						parent_gifts.add(child_)
						if child_ in COUPLES: 
							parent_gifts.add(COUPLES[child_])

				'''for child in PC[parent_spouse]: 
					if child != giver and child != giver_spouse: 
						parent_gifts.append(child)
						if child in COUPLES: 
							parent_gifts.append(COUPLES[child])'''
			
				#print("PARENTS of children to buy gifts for: ", parent_gifts)

				# Get children to buy gifts for
				for parent_ in parent_gifts: 	
					for cp in PC[parent_]: 
						if cp not in children_gifts: 
							children_gifts.add(cp) 
							if cp in COUPLES and COUPLES[cp] not in children_gifts: 
								children_gifts.add(COUPLES[cp])
			
			if len(children_gifts): 
				print(giver + " needs to buy gifts for:", ', '.join(sorted(list(children_gifts)))) 		
			else: 
				print(giver + " does not need to buy gifts") 				
