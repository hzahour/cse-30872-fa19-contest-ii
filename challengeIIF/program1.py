#!/usr/bin/env python3
import sys
import collections 

def get_level(g, query): 
	
	for key in g: 
		if (key[0] == query[0] and key[1] == query[1]) or (key[0] == query[1] and key[1] == query[0]): 
			return key[2]
	return 1

def create_graph(n_fams): 

	g = collections.defaultdict(set)
	parents_pairings = {}

	# Save original input
	org_input = [] 
	for i in range(n_fams):
		org_input.append(sys.stdin.readline().strip('\n'))

	# Get and store parent pairs 
	for line in org_input: 
		#parents, children = line.split(': ')
		#parents 					= parents.split(' ') 

		m 								= line.split(': ')
		parents 					= m[0]
		parents 					= parents.split(' ') 


		if len(m) < 2: 
			children 				= []
		else: 
			children				= m[1]

		parents_pairings[parents[0]] = parents[1]
		parents_pairings[parents[1]] = parents[0]

	# Populate adjacency list 
	for line in org_input: 
		 
		m 								= line.split(': ')
		parents 					= m[0]

		if len(m) < 2: 
			children 				= ""
		else: 
			children				= m[1]

		parents 					= parents.split(' ') 
		children 					= children.split(' ')
		
		# Get level for parents
		level = get_level(g, parents)
		parents = (parents[0], parents[1], level)

		# Add each edge, along with level, to the adjacency list 
		for child in children: 

			child_pair = None 

			if child in parents_pairings: 
				child_pair = parents_pairings[child]

			child_level = get_level(g, (child, child_pair))				

			if child_level <= level:
				g[(child, child_pair, level + 1)] = g[(child, child_pair, child_level)]
				g.pop((child, child_pair, child_level), None)
					
				for sub_child in g[(child, child_pair, level + 1)]:

					sub_child_level = get_level(g, sub_child)

					g[(sub_child[0], sub_child[1], level + 2)] = g[(sub_child[0], sub_child[1], sub_child_level)] 
					g.pop((sub_child[0], sub_child[1], sub_child_level), None)

			g[parents].add((child, child_pair))		
			g[(child, child_pair, level + 1)].add((parents[0], parents[1]))
		
	return g, parents_pairings

def niblings(giver, g, p_pairings):

	res = [] 
		
	spouse = None
	if giver in p_pairings: 
		spouse = p_pairings[giver]

	level = get_level(g, (giver, spouse))
		
	for parents in g: 
		if (parents[2] == level):
			if (parents != (spouse, giver, level) and  parents != (giver, spouse, level)): 
				for child in g[parents]:
					# Check level of child
					child_level = get_level(g, child)
					if child_level > level:
						res.append(child[0])
	
	return sorted(res)

if __name__=="__main__": 

	# Read in input 
	for line in sys.stdin: 

		n_fams = int(line.strip('\n'))
		g, p_pairings = create_graph(n_fams)
		n_givers = int(sys.stdin.readline().strip('\n'))

		for _ in range(n_givers): 

			giver = sys.stdin.readline().strip('\n')
			nn = niblings(giver, g, p_pairings)

			if nn == ['']:
				print(giver + " does not need to buy gifts")
			else : 
				print(giver + " needs to buy gifts for: " + ', '.join(nn))

		
		

